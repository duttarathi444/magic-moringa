'use strict';

module.exports = {
    tag: { name: 'Cart', description: 'Cart APIs' },
    paths: {
        '/v1/cart/addCart': {
            post: {
                tags: ['Cart'],
                summary: 'create or update Cart',
                operationId: 'Cart_addCart',
                parameters: [
                    {
                        name: 'body',
                        in: 'body',
                        required: true,
                        schema: { '$ref': '#/definitions/CartCreateOrUpdate' },
                    },
                ],
                responses: {
                    '200': { description: 'Success response' },
                },
            }
        },
    },
    definitions: {
        CartCreateOrUpdate: {
            type: 'object',
            properties: {
                user_id: {
                    type: 'integer',
                },
                cart_details: {
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            prod_id: {
                                type: "integer",
                            },
                            quantity: {
                                type: "integer"
                            }
                        }
                    }
                },
            },
        },
    },
}