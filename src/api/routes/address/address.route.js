'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const {
    addAddress,
    editAddress,
    deleteAddress,
    addressList
} = require('../../controllers').AddressController;

const { authenticate, authorize } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;

const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;

/**********************************************
 * ProductMaster Model APIs v1
 *
 *******************************/
router.post('/api/v1/address/addAddress', catchErrors(addAddress));
router.post('/api/v1/address/editAddress', catchErrors(editAddress));
router.post('/api/v1/address/deleteAddress', catchErrors(deleteAddress));
router.get('/api/v1/address/addressList/:customer_id', catchErrors(addressList));