'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const { misUserLogin, misUserLogout, customerList, saleOrdersList, saleOrderDetails, getStoreNextOrderStatusList, saleOrderStatusUpdate, productAdd, storeList, productList, uomList } = require('../../controllers').MisUserController;
const { authenticate, authorize, authorizeMisUser } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;
const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;


/*********************************
 * ProductMaster Model APIs v1
 *
 *******************************/

router.post('/api/v1/employee/misuser/login', catchErrors(misUserLogin));
router.post('/api/v1/employee/misuser/logout', catchErrors(misUserLogout));
router.post('/api/v1/employee/misuser/customerList', catchErrors(customerList));
router.post('/api/v1/employee/misuser/saleOrdersList', catchErrors(saleOrdersList));
router.post('/api/v1/employee/misuser/saleOrderDetails', catchErrors(saleOrderDetails));
router.post('/api/v1/employee/misuser/getStoreNextOrderStatusList', catchErrors(getStoreNextOrderStatusList));
router.post('/api/v1/employee/misuser/saleOrderStatusUpdate', catchErrors(saleOrderStatusUpdate));

router.post('/api/v1/employee/misuser/productAdd', catchErrors(productAdd));
router.get('/api/v1/employee/misuser/storeList', catchErrors(storeList));
router.post('/api/v1/employee/misuser/productList', catchErrors(productList));

router.get('/api/v1/employee/misuser/uomList', catchErrors(uomList));

