'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const {
    newOrderList,
    laundrymanLogin,
    laundrymanLogout,
    acceptOrder,
    acceptedOrderList,
    completeOrder,
    completedOrderList,
    addEditWorkLocation,
    viewWorkLocation

} = require('../../controllers').LaundryManController;
const { authenticate, authorize, authorizeLaundrymanOrDeliveryBoy } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;
const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;


/**********************************************
 * ProductMaster Model APIs v1
 *
 *******************************/

router.post('/api/v1/employee/laundryman/login', catchErrors(laundrymanLogin));
router.post('/api/v1/employee/laundryman/logout', authorizeLaundrymanOrDeliveryBoy(), catchErrors(laundrymanLogout));
router.post('/api/v1/employee/laundryman/newOrderList', authorizeLaundrymanOrDeliveryBoy(), catchErrors(newOrderList));

router.post('/api/v1/employee/laundryman/acceptOrder', authorizeLaundrymanOrDeliveryBoy(), catchErrors(acceptOrder));
router.post('/api/v1/employee/laundryman/acceptedOrderList', authorizeLaundrymanOrDeliveryBoy(), catchErrors(acceptedOrderList));
router.post('/api/v1/employee/laundryman/completeOrder', authorizeLaundrymanOrDeliveryBoy(), catchErrors(completeOrder));
router.post('/api/v1/employee/laundryman/completedOrderList', authorizeLaundrymanOrDeliveryBoy(), catchErrors(completedOrderList));

router.post('/api/v1/employee/laundryman/addEditWorkLocation', authorizeLaundrymanOrDeliveryBoy(), catchErrors(addEditWorkLocation));
router.post('/api/v1/employee/laundryman/viewWorkLocation', authorizeLaundrymanOrDeliveryBoy(), catchErrors(viewWorkLocation));