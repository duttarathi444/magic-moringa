'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const {
    deliveryboyLogin,
    deliveryboyLogout,
    newOrderList,
    acceptOrder,
    acceptedCustomerOrderList,
    acceptedLaundrymanOrderList,
    deliverOrder
} = require('../../controllers').DeliveryBoyController;
const { authenticate, authorize, authorizeLaundrymanOrDeliveryBoy } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;
const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;


/**********************************************
 * ProductMaster Model APIs v1
 *
 *******************************/

router.post('/api/v1/employee/deliveryboy/login', catchErrors(deliveryboyLogin));
router.post('/api/v1/employee/deliveryboy/logout', authorizeLaundrymanOrDeliveryBoy(), catchErrors(deliveryboyLogout));
router.post('/api/v1/employee/deliveryboy/newOrderList', catchErrors(newOrderList));
router.post('/api/v1/employee/deliveryboy/acceptOrder', authorizeLaundrymanOrDeliveryBoy(), catchErrors(acceptOrder));
router.post('/api/v1/employee/deliveryboy/acceptedCustomerOrderList', authorizeLaundrymanOrDeliveryBoy(), catchErrors(acceptedCustomerOrderList));
router.post('/api/v1/employee/deliveryboy/acceptedLaundrymanOrderList', authorizeLaundrymanOrDeliveryBoy(), catchErrors(acceptedLaundrymanOrderList));
router.post('/api/v1/employee/deliveryboy/deliverOrder', authorizeLaundrymanOrDeliveryBoy(), catchErrors(deliverOrder));
