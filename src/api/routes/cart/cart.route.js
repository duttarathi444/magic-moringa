'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const {
    addEditDeleteCart,
    findCart
} = require('../../controllers').CartController;
const { authenticate, authorize } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;

const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;

/*******************************************
 * ProductMaster Model APIs v1
 *
 *******************************/
/*
router.post('/api/v1/cart/addEditDeleteCart', authorize(), catchErrors(addEditDeleteCart));
//router.post('/api/v1/cart/editdeleteCart', editdeleteCart);
router.get('/api/v1/cart/findCart/:user_id', authenticate(), catchErrors(findCart));
*/

//test
router.post('/api/v1/cart/addEditDeleteCart', catchErrors(addEditDeleteCart));
router.get('/api/v1/cart/findCart/:customer_id', catchErrors(findCart));

