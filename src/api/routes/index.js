'use strict';

// local imports
const homeRoutes = require('./home.route');
const userRoutes = require('./user/user.route');
const productRoutes = require('./product/product.route');
const cartRoutes = require('./cart/cart.route');
const OrderRoutes = require('./order/order.route');
const AddressRoutes = require('./address/address.route');
const LaundryManRoutes = require('./employee/laundry_man.route');
const DeliveryBoyRoutes = require('./employee/delboy.route');
const MisUserRoutes = require('./employee/misuser.route');


// configure all routes
module.exports = (app) => {
    app.use(homeRoutes);
    app.use(userRoutes);
    app.use(productRoutes);
    app.use(cartRoutes);
    app.use(OrderRoutes);
    app.use(AddressRoutes);
    app.use(LaundryManRoutes);
    app.use(DeliveryBoyRoutes);
    app.use(MisUserRoutes);

}