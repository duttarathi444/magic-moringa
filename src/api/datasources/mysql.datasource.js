'use strict';

// global imports
const Sequelize = require("sequelize");
// local imports
const env = require('../../env');

/**
 * sequelize settings
 */
// mongoose.Promise = global.Promise;
// if (env.env.isMysqlDebugMode_ON) {
//     mongoose.set('debug', true);
// }

/**
 * mysql settings
 */
const dbConfig = {
    default: 'db1',
    db1: {
        uri: env.mysql.URI,
        name: "moringa_db",
        username: "moringauser2021",
        password: "moringauser2021",
        options: {
            host: 'localhost',
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000000000,
                operatorsAliases: false
            }
        }
    },
    // db2: {
    //     uri: env.mysql.URI2,
    //     options: undefined
    // },
};

const defaultOptions = {
};

/**
 * helper functions
 */
function createConnection(dbName) {
    console.log('\nMysqlDataSource.createConnection triggered');

    const { uri, name, username, password, options } = dbConfig[dbName];

    // create mongo connection
    const conn = new Sequelize(name, username, password, options);
    conn
        .authenticate()
        .then(() => {
            console.log('Connection has been established successfully.');
        })
        .catch(err => {
            console.error('Unable to connect to the database:', err);
        });

    /**
     * Events
     */
    conn.beforeConnect((config) => {
        console.log("beforeConnect hook triggered......");
    });
    conn.afterConnect(conn, (config) => {
        console.log("afterConnect hook triggered......");
    })
    conn.beforeDisconnect((conn) => {
        console.log("beforeDisconnect hook triggered......");
    });
    conn.afterDisconnect((conn) => {
        console.log("afterDisconnect hook triggered......");
    });

    return conn;
}

async function createConnections(dbNames = []) {
    console.log('\nMysqlDataSource.createConnections triggered');

    const dbs = {};

    await Promise.all(dbNames.map(async (dbName) => {
        dbs[dbName] = await createConnection(dbName);
    }));

    return dbs;
}

/**
 * Exports
 */
// exports.createConnection = createConnection;
exports.createConnections = createConnections;
exports.SequelizeOp = Sequelize.Op;

