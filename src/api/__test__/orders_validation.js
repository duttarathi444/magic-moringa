let orderRequests = [{
    "cart_id": 11,
    "user_id": 5,
    "order_date": "2019-12-30 06:47:00",
    "order_details": [
        {
            "category_id": 1,
            "prod_id": 2,
            "quantity": 12,
            "per_unit_price": "5.00",
            "total_price": "60.00"
        },
        {
            "category_id": 1,
            "prod_id": 3,
            "quantity": 5,
            "per_unit_price": "8.00",
            "total_price": "40.00"
        }
    ],
    "grand_total_quantity": 17,
    "grand_total_price": "100.00",
    "address": {
        "address1": "Lane-1",
        "address2": "Block-1",
        "address3": "Street-1",
        "city": "Cuttack",
        "state": "Odisha",
        "pincode": "753001",
        "longitude": "111111111.00001",
        "latitude": "2222222222.00002",
        "flag_default": "primary",
        "phoneno": "9338468609"
    }
},
{
    "cart_id": 11,
    "user_id": 5,
    "order_date": "2019-12-30 06:47:00",
    "order_details": [

        {
            "category_id": 2,
            "prod_id": 22,
            "quantity": 25,
            "per_unit_price": "5.00",
            "total_price": "125.00"
        },
        {
            "category_id": 2,
            "prod_id": 23,
            "quantity": 10,
            "per_unit_price": "8.00",
            "total_price": "80.00"
        }
    ],
    "grand_total_quantity": 35,
    "grand_total_price": "205.00",
    "address": {
        "address1": "Lane-1",
        "address2": "Block-1",
        "address3": "Street-1",
        "city": "Cuttack",
        "state": "Odisha",
        "pincode": "753001",
        "longitude": "111111111.00001",
        "latitude": "2222222222.00002",
        "flag_default": "primary",
        "phoneno": "9338468609"
    }
}];

const categoryDetails = [
    { category_id: 1, category_name: "Ironing", min_price: 99 },
    { category_id: 2, category_name: "Washing and Ironing", min_price: 399 },
    { category_id: 3, category_name: "Dry Cleaning", min_price: 299 },
];
const categroyMinPrices = {};

categoryDetails.forEach(categoryDetail => {
    categroyMinPrices[categoryDetail.category_id] = {
        name: categoryDetail.category_name,
        value: categoryDetail.minPrice,
    };
});

console.log("categroyMinPrices===", categroyMinPrices);
/* const categoryMinPrices = {
    "1": {
        name: "Ironing",
        value: 99,
    },
    "2": {
        name: "Wash & Ironing",
        value: 199,
    },
    "3": {
        name: "Dry Cleaning",
        value: 399,
    },
};
 */

let minPriceFlag = false;
/* orderRequests.forEach(orderRequest => {

    const categoryId = orderRequest.order_details[0].category_id;
    let errMsg = "";
    if (orderRequest.grand_total_price < categoryMinPrices[categoryId]) {
        minPriceFlag = false;
        errMsg = `Minimu price of category `
        break;
    }
    minPriceFlag = true;
}); */
let errMsg = "";
for (let i = 0; i < orderRequests.length; i++) {
    const categoryId = orderRequests[i].order_details[0].category_id;

    if (orderRequests[i].grand_total_price < categroyMinPrices[categoryId].value) {
        minPriceFlag = false;
        errMsg = `Minimum price of category ${categroyMinPrices[categoryId].name} `
        break;
    }
    minPriceFlag = true;
}
if (!minPriceFlag) {
    //return res.send("Minimum pricing Error");
    console.log("\nMinimum pricing Error");
    console.log(errMsg);
}

