const Sequelize = require("sequelize");

const categoryPricesSchema = {
    schema: {
        // attributes
        category_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        category_name: {
            type: Sequelize.STRING
        },
        min_price: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = categoryPricesSchema;