const Sequelize = require("sequelize");

const productStockSchema = {
    schema: {
        // attributes
        product_stock_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        store_id: {
            type: Sequelize.INTEGER
        },
        sale_point_id: {
            type: Sequelize.INTEGER
        },
        current_stock: {
            type: Sequelize.DECIMAL
        },
        sku: {
            type: Sequelize.INTEGER
        },
        prod_id: {
            type: Sequelize.INTEGER
        },
        total_waste: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = productStockSchema;