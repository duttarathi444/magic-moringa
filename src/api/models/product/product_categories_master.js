const Sequelize = require("sequelize");

const productCategoriesMasterSchema = {
    schema: {
        // attributes
        category_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        category_name: {
            type: Sequelize.STRING
        },
        category_desc: {
            type: Sequelize.STRING
        },
        parent_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = productCategoriesMasterSchema;