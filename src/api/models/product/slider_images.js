const Sequelize = require("sequelize");

const sliderImageSchema = {
    schema: {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        slider_id: {
            type: Sequelize.INTEGER
        },
        slider_image_path: {
            type: Sequelize.STRING,
            len: [1, 300]
        },

    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = sliderImageSchema;