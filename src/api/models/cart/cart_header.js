const Sequelize = require("sequelize");

const cartHeaderSchema = {
    schema: {
        // attributes
        cart_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        cart_no: {
            type: Sequelize.INTEGER,
            required: true
        },
        customer_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = cartHeaderSchema;