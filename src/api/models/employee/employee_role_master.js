const Sequelize = require("sequelize");

const employeeRoleMasterSchema = {
    schema: {
        // attributes
        emp_role_master_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        emp_role_name: {
            type: Sequelize.STRING
        },
        emp_role_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,

    }
}

module.exports = employeeRoleMasterSchema;