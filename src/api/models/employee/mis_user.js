const Sequelize = require("sequelize");

const misUserSchema = {
    schema: {
        // attributes
        mis_user_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        mis_user_fname: {
            type: Sequelize.STRING,
            isAlphanumeric: true,
            required: true,
            allowNull: false,
            len: [8, 20]
        },
        mis_user_lname: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false
        },
        mis_user_email_id: {
            type: Sequelize.STRING,
            validate: {
                isEmail: {
                    msg: "Must be an email"
                }
            }
        },
        mis_user_contact_no: {
            type: Sequelize.STRING
        },
        mis_user_role_id: {
            type: Sequelize.INTEGER
        },
        mis_user_status: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = misUserSchema;