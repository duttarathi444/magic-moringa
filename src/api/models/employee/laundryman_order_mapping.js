const Sequelize = require("sequelize");

const laundrymanOrderMappingSchema = {
    schema: {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        order_id: {
            type: Sequelize.INTEGER
        },
        emp_id: {
            type: Sequelize.INTEGER
        },
        allocation_date: {
            type: Sequelize.STRING
        },
        allocation_time: {
            type: Sequelize.STRING
        },
        active_status: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = laundrymanOrderMappingSchema;