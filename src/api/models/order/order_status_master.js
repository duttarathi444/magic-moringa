const Sequelize = require("sequelize");

const orderStatusMasterSchema = {
    schema: {
        // attributes
        order_status_master_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        store_id: {
            type: Sequelize.INTEGER
        },
        status_id: {
            type: Sequelize.INTEGER
        },
        status_desc: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = orderStatusMasterSchema;
