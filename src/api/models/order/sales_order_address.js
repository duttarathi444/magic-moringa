const Sequelize = require('sequelize');

const salesOrderAddressSchema = {
    schema: {
        order_address_id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        address1: {
            type: Sequelize.STRING
        },
        address2: {
            type: Sequelize.STRING
        },
        address3: {
            type: Sequelize.STRING
        },
        city: {
            type: Sequelize.STRING
        },
        state: {
            type: Sequelize.STRING
        },
        pincode: {
            type: Sequelize.INTEGER
        },
        longitude: {
            type: Sequelize.STRING
        },
        latitude: {
            type: Sequelize.STRING
        },
        flag_default: {
            type: Sequelize.STRING
        },
        customer_id: {
            type: Sequelize.INTEGER
        },
        store_id: {
            type: Sequelize.INTEGER
        },
        phone_no: {
            type: Sequelize.STRING
        },
        sales_order_id: {
            type: Sequelize.INTEGER
        }
    }, options: {
        timestamps: false,
        freezeTableName: true
    }
}

module.exports = salesOrderAddressSchema;