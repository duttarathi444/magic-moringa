const Sequelize = require("sequelize");

const orderDetailsSchema = {
    schema: {
        // attributes
        order_details_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        order_id: {
            type: Sequelize.INTEGER
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        prod_id: {
            type: Sequelize.INTEGER
        },
        customer_order_quantity: {
            type: Sequelize.INTEGER
        },
        dboy_order_accept_quantity: {
            type: Sequelize.INTEGER
        },
        lman_order_accept_quantity: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = orderDetailsSchema;