const Sequelize = require("sequelize");

const orderHistorySchema = {
    schema: {
        // attributes
        order_his_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        order_id: {
            type: Sequelize.INTEGER
        },
        status_id: {
            type: Sequelize.INTEGER
        },
        status_date: {
            type: Sequelize.STRING
        },
        active_flag: {
            type: Sequelize.STRING
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        emp_id: {
            type: Sequelize.INTEGER
        },
        role_id: {
            type: Sequelize.INTEGER
        },
        status_time: {
            type: Sequelize.STRING
        },
        item_count: {
            type: Sequelize.INTEGER
        },
        total_price: {
            type: Sequelize.DECIMAL
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = orderHistorySchema;