const Sequelize = require('sequelize');

const salesOrderHistory = {
    schema: {
        order_his_id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        order_id: {
            type = Sequelize.INTEGER
        },
        status_id: {
            type = Sequelize.INTEGER
        },
        status_date: {
            type = Sequelize.STRING
        },
        active_flag: {
            type: Sequelize.STRING
        },
        customer_id: {
            type: Sequelize.INTEGER
        },
        emp_id: {
            type: Sequelize.INTEGER
        },
        role_id: {
            type = Sequelize.INTEGER
        },
        status_time: {
            type = Sequelize.STRING
        },
        item_count: {
            type: Sequelize.INTEGER
        },
        total_price: {
            type: Sequelize.DECIMAL(10, 2)
        }
    }, options: {
        timestamps: false,
        freezeTableName: true
    }
}

module.exports = salesOrderHistory;