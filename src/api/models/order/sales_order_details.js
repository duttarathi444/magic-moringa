const Sequelize = require("sequelize");

const salesOrderDetailsSchema = {
    schema: {
        // attributes
        sales_order_details_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        sales_order_id: {
            type: Sequelize.INTEGER
        },
        customer_id: {
            type: Sequelize.INTEGER
        },
        prod_id: {
            type: Sequelize.INTEGER
        },
        customer_order_quantity: {
            type: Sequelize.INTEGER
        },
        dboy_order_accept_quantity: {
            type: Sequelize.INTEGER
        },
        lman_order_accept_quantity: {
            type: Sequelize.INTEGER
        },
        store_id: {
            type: Sequelize.INTEGER
        },
        customer_order_price: {
            type: Sequelize.DECIMAL(10, 2)
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = salesOrderDetailsSchema;