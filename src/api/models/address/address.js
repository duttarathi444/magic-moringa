const Sequelize = require("sequelize");

const addressSchema = {
    schema: {
        // attributes
        address_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        address1: {
            type: Sequelize.STRING
        },
        address2: {
            type: Sequelize.STRING
        },
        address3: {
            type: Sequelize.STRING
        },
        state: {
            type: Sequelize.STRING
        },
        city: {
            type: Sequelize.STRING
        },
        pincode: {
            type: Sequelize.STRING
        },
        longitude: {
            type: Sequelize.STRING
        },
        latitude: {
            type: Sequelize.STRING
        },
        flag_default: {
            type: Sequelize.STRING
        },
        customer_id: {
            type: Sequelize.INTEGER
        },
        phone_no: {
            type: Sequelize.STRING
        }

    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = addressSchema;