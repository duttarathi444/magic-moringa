const Sequelize = require("sequelize");

const customerMasterSchema = {
    schema: {
        // attributes
        customer_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        customer_auth: {
            type: Sequelize.STRING
        },
        login_type: {
            type: Sequelize.STRING
        },
        customer_fname: {
            type: Sequelize.STRING
        },
        customer_lname: {
            type: Sequelize.STRING
        },
        customer_email: {
            type: Sequelize.STRING
        },
        customer_phoneno: {
            type: Sequelize.STRING
        },
        created_at: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW
        },
        time: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = customerMasterSchema;