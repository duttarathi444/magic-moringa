const Sequelize = require("sequelize");

const phonenoOtpSchema = {
    schema: {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        phoneno: {
            type: Sequelize.STRING
        },
        otp: {
            type: Sequelize.STRING
        },
        otp_creation_time: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = phonenoOtpSchema;