const Sequelize = require("sequelize");

const loginSchema = {
    schema: {
        // attributes
        login_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        customer_id: {
            type: Sequelize.INTEGER
        },
        password: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        },
        last_access_date: {
            type: Sequelize.STRING
        },
        fcmToken: {
            type: Sequelize.STRING
        },
        app_version_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = loginSchema;