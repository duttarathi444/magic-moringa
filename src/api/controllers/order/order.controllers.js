const moment = require('moment');
const HttpStatus = require('http-status-codes');
const axios = require('axios');
const https = require('https');

const env = require('../../../env');
const { getModels, SequelizeOp } = require('../../models');
const { json } = require('express');
const { where } = require('sequelize');

let orderHeader;
let address;
const utcOffsetMins = env.env.utcOffsetMins;
const order_creation_id = env.service_flow.order_creation_id;

exports.createMultipleOrders = async (req, res) => {
    console.log('\norder.controller.createMultipleOrders  triggered -->');

    const reqBody = req.body;
    // const {
    //     OrderNotifications, Login, EmployeeLogin, UserMaster, DelboyOrderMapping, Employee,
    //     OrderHeader, OrderDetails, OrderHistory, Address, LaundryServiceFlow, CartDetails,
    //     CartHeader, ServiceAvailabilityPincode, db1Conn
    // } = await getModels();
    const {
        OrderNotifications, Login, ProductStock, OrderAddress,
        OrderHeader, OrderDetails, OrderHistory, Address, LaundryServiceFlow, CartDetails,
        CartHeader, ProductStockMove, db1Conn
    } = await getModels();
    const orderHeaders = [];
    // validate orderRequests
    const validationErros = await validateOrderRequests(reqBody, "create");
    console.log("\n\n validationErros======", validationErros);
    if (validationErros.length) {
        return res.send({ status: 0, message: validationErros[0], data: [{}] })
    }
    // generate unique paytm virtual id  
    // const virtual_order_id = new Date().getTime();
    const t = await db1Conn.transaction();

    try {
        // create all new orders
        await Promise.all(reqBody.map(async (orderRequest) => {
            console.log("\n\n ORDER_REQUEST_DETAIL======", JSON.stringify(orderRequest, 0, 2));

            // Here status_id and module_id are hard coded, it will be dynamic in future
            // let laundry_service_flow = await LaundryServiceFlow.findOne({
            //     where: { status_id: order_creation_id, module_id: 1 }
            // }, { transaction: t });
            // console.log("\n\n LAUNDRY SERVICE FLOW======", JSON.stringify(laundry_service_flow, 0, 2));
            const format = 'hh:mm:ss A';

            const startMoment_1 = moment('12:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_1 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            console.log("\n\n startMoment_1===", startMoment_1);
            console.log("\n\n endMoment_1===", endMoment_1);

            const startMoment_2 = moment('08:00:00 AM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_2 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_3 = moment('12:00:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_3 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);

            const startMoment_4 = moment('18:30:00 PM', format).utc().utcOffset(utcOffsetMins);
            const endMoment_4 = moment('23:59:59 PM', format).utc().utcOffset(utcOffsetMins);

            let order_receive_date_time = "";
            const order_moment = moment().utc().utcOffset(utcOffsetMins);
            const cancel_order_moment = order_moment.clone().add(15, 'minutes');

            if (order_moment.isSameOrAfter(startMoment_1) && order_moment.isBefore(endMoment_1)) {
                // const delivery_date_string = order_moment.clone().format("DD/MM/YYYY");
                // const delivery_time_string = '10:00 am';
                // order_receive_date_time = delivery_date_string + " " + delivery_time_string;
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time=", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_2) && order_moment.isBefore(endMoment_2)) {
                // order_receive_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
                order_receive_date_time = "Thanks for your order. Your clothes will be picked up by our delivery partner today between 8:00 AM and 1:30 PM";
                console.log("\n\n order_receive_date_time==", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_3) && order_moment.isBefore(endMoment_3)) {
                // order_receive_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time===", order_receive_date_time);
            }
            else if (order_moment.isSameOrAfter(startMoment_4) && order_moment.isBefore(endMoment_4)) {
                // const delivery_date_string = order_moment.clone().add(1, 'day').format("DD/MM/YYYY");
                // console.log("\n\n delivery_date_string ======333", delivery_date_string);
                // const delivery_time_string = '10:00 am';
                // order_receive_date_time = delivery_date_string + " " + delivery_time_string;
                order_receive_date_time = "Dear customer Thanks for your order however you just missed our same day delivery window. However we are trying our best to pick up your order at the earliest. ETA Tomorrow 10 AM";
                console.log("\n\n order_receive_date_time====", order_receive_date_time);
            }
            let order_date_time = order_moment.format("DD/MM/YYYY hh:mm A");
            let order_date_time_array = order_date_time.split(" ");
            let order_date = order_date_time_array[0];
            let order_time = order_date_time_array[1] + " " + order_date_time_array[2];
            let cancel_date_time = cancel_order_moment.format("DD/MM/YYYY hh:mm A");
            let cancel_date_time_array = cancel_date_time.split(" ");
            let cancel_date = cancel_date_time_array[0];
            let cancel_time = cancel_date_time_array[1] + " " + cancel_date_time_array[2];
            console.log("\n\n order_date_time ======", order_date_time);
            console.log("\n\n order_date  ======", order_date);
            console.log("\n\n order_time  ======", order_time);
            console.log("\n\n cancel_date_time ======", cancel_date_time);
            console.log("\n\n cancel_date  ======", cancel_date);
            console.log("\n\n cancel_time  ======", cancel_time);
            console.log("\n\n ORDER MONENT ======", order_moment);
            console.log("\n\n CANCEL  ORDER MONENT ======", cancel_order_moment);
            console.log("\n\n DELIVERY ORDER MONENT ======", order_receive_date_time);

            // let statusDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm:ss a");
            // let date_time_array = statusDate.split(" ");
            // let date = date_time_array[0];
            // let time = date_time_array[1] + " " + date_time_array[2];

            // const orderHeader = await OrderHeader.create({
            //     user_id: orderRequest.user_id, trans_id: orderRequest.trans_id, order_date: order_date_time,
            //     status_id: laundry_service_flow.status_id, item_count: orderRequest.grand_total_quantity,
            //     total_price: orderRequest.grand_total_price, address: orderRequest.address, expected_delivery_time: " ",
            //     actual_delivery_time: " ", pincode: orderRequest.address.pincode,category_id: orderRequest.order_details[0].category_id
            // }, { transaction: t });

            const orderHeader = await OrderHeader.create({
                customer_id: orderRequest.customer_id,
                trans_id: orderRequest.trans_id,
                order_date: order_date,
                status_id: 1000,
                item_count: orderRequest.grand_total_quantity,
                total_price: orderRequest.grand_total_price,
                expected_delivery_time: " ",
                actual_delivery_time: " ",
                pincode: orderRequest.address.pincode,
                store_id: orderRequest.store_id
            }, { transaction: t });

            console.log("\n\n ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));

            // stock decrease from product stock and entry in stock movement table
            await Promise.all(orderRequest.order_details.map(async (element) => {
                let decreaseStock = await ProductStock.decrement({ current_stock: element.quantity },
                    { where: { store_id: orderRequest.store_id, prod_id: element.prod_id } },
                    { transaction: t });
                console.log(decreaseStock);
                let stockMovement = await ProductStockMove.create({
                    store_id: orderRequest.store_id,
                    product_id: element.prod_id,
                    mov_type: 2,
                    mov_quantity: '-' + element.quantity,
                    mov_sku: 1,
                    sales_order_id: orderHeader.sales_order_id,
                    sales_date: order_date,
                    sales_time: order_time,
                    close_stock: 1,
                    close_stock_sku: 1,
                    open_stock: 1,
                    open_stock_sku: 1
                }, { transaction: t });
            }));

            console.log('Product stock movement added.');

            if (orderRequest.address.address_id) {
                let address = await OrderAddress.findOne({
                    where: { order_address_id: orderRequest.address.address_id },
                    attributes: ['address1', 'address2', 'address3', 'city', 'state', 'pincode',
                        'longitude', 'latitude', 'flag_default', 'phone_no']
                });
                console.log("\n\n EXISTING ADDRESS ======", JSON.stringify(address, 0, 2));

                await OrderAddress.update({ flag_default: 'secondary' },
                    { where: { customer_id: orderRequest.customer_id } }, { transaction: t });

                await OrderAddress.update({ flag_default: orderRequest.address.flag_default },
                    { where: { order_address_id: orderRequest.address.address_id } }, { transaction: t });
            }
            else {
                // await Address.update({ flag_default: 'secondary' },
                //     { where: { user_id: orderRequest.user_id } }, { transaction: t });

                const address = await OrderAddress.create({
                    address1: orderRequest.address.address1,
                    address2: orderRequest.address.address2,
                    address3: orderRequest.address.address3,
                    city: orderRequest.address.city,
                    state: orderRequest.address.state,
                    pincode: orderRequest.address.pincode,
                    longitude: orderRequest.address.longitude,
                    latitude: orderRequest.address.latitude,
                    flag_default: orderRequest.address.flag_default,
                    customer_id: orderRequest.customer_id,
                    phone_no: orderRequest.address.phoneno,
                    store_id: orderRequest.store_id,
                    sales_order_id: orderHeader.sales_order_id
                }, { transaction: t });
                console.log("\n\n ADDRESS CREATED======", JSON.stringify(address, 0, 2));
            }

            await Promise.all(orderRequest.order_details.map(async (element) => {
                console.log("\n\n ORDER_DETAIL======", JSON.stringify(element, 0, 2));
                let orderDetails = await OrderDetails.create({
                    sales_order_id: orderHeader.sales_order_id,
                    customer_id: orderHeader.customer_id,
                    prod_id: element.prod_id,
                    customer_order_quantity: element.quantity,
                    customer_order_price: element.total_price,
                    dboy_order_accept_quantity: 0,
                    lman_order_accept_quantity: 0,
                    store_id: orderRequest.store_id
                }, { transaction: t });
                console.log("\n\n ORDER_DETAIL ADDED SUCCESSFULLY ")
            }));
            console.log("\n\n ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));
            console.log("\n\n ORDERHEADER ITEM COUNT ======", orderHeader.item_count);
            console.log("\n\n ORDERHEADER ITEM PRICE ======", orderHeader.total_price);
            console.log("\n\n ORDER DATE ======", order_date);
            console.log("\n\n ORDER TIME ======", order_time);

            // const orderHistory = await OrderHistory.create({
            //     order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
            //     status_date: order_date, active_flag: laundry_service_flow.status_desc,
            //     customer_id: orderRequest.customer_id, status_time: order_time, item_count: orderHeader.item_count,
            //     total_price: orderHeader.total_price
            // }, { transaction: t });
            // const orderHistory = await OrderHistory.create({
            //     order_id: orderHeader.sales_order_id,
            //     status_id: 1,
            //     status_date: order_date,
            //     active_flag: '',
            //     customer_id: orderRequest.customer_id,
            //     status_time: order_time,
            //     item_count: orderHeader.item_count,
            //     total_price: orderHeader.total_price,
            // }, { transaction: t });

            // console.log("\n\n ORDER HISTORY CREATED======", JSON.stringify(orderHistory, 0, 2));

            console.log("\n\n ORDER ADDED SUCCESSFULLY ");
            orderHeader.setDataValue('cancel_date_time', cancel_date_time);
            orderHeader.setDataValue('order_receive_date_time', order_receive_date_time);

            orderHeaders.push(orderHeader);
        }));

        // delete the cart
        const cartDetails = await CartDetails.findAll({
            where: { cart_id: reqBody[0].cart_id }
        });
        await Promise.all(cartDetails.map(async (element) => {
            await CartDetails.destroy({ where: { cart_id: element.cart_id }, transaction: t });
        }));
        await CartHeader.destroy({
            where: { customer_id: reqBody[0].customer_id, cart_id: reqBody[0].cart_id }, transaction: t
        });

        // commit txn
        t.commit();

        // send all order related notifications
        // await Promise.all(orderHeaders.map(async (orderHeader) => {
        //     try {
        //         let x = 0;
        //         let refreshId = setInterval(async function () {
        //             x = x + 1;
        //             console.log("\n\n TRY TO SEND MESSAGE FOR TIME :" + x);
        //             let result = await sendSMS(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
        //             let result_1 = await sendSMSDemo(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
        //             if (result) {
        //                 console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
        //                 console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO ADMIN ", result_1);
        //                 clearInterval(refreshId);
        //             }
        //             if (x === 3) {
        //                 console.log("\n\nTRY FOR TRHEE TIMES, MESSAGE NOT SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
        //                 clearInterval(refreshId);
        //             }
        //         }, 5000);

        //         let empname = "";
        //         let emp_phoneno = "";
        //         let msg_body1 = "";
        //         let login = await Login.findOne({ where: { user_id: orderHeader.user_id } });
        //         let user = await UserMaster.findOne({ where: { user_id: orderHeader.user_id } });
        //         let dboy_order_mapping = await DelboyOrderMapping.findOne({
        //             where: { order_id: orderHeader.order_id, delivery_type: 1 }
        //         });
        //         if (dboy_order_mapping) {
        //             let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
        //             empname = employee.emp_fname + " " + employee.emp_lname;
        //             emp_phoneno = employee.emp_contact_no;
        //         }

        //         let employee_login = await EmployeeLogin.findOne({
        //             where: { emp_id: dboy_order_mapping.emp_id }
        //         });
        //         const msg_body = 'Thanks for ordering. Order no ' + orderHeader.order_id +
        //             ' will be pick up by our delivery boy ' + empname + ', phone no-' + emp_phoneno;

        //         let customername = user.user_fname + " " + user.user_lname;
        //         let customerphoneno = "";
        //         if (user.user_phoneno) {
        //             console.log("\nDELIVERYBOY NOTIFICATION USER PHONE NO :" + user.user_phoneno);
        //             msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
        //             customerphoneno = user.user_phoneno;
        //         }
        //         else {
        //             console.log("\nDELIVERYBOY NOTIFIACTION USER HAVING NO PHONE NO :");
        //             msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
        //             customerphoneno = "";
        //         }

        //         let laundry_service_flow = await LaundryServiceFlow.findOne({
        //             where: { status_id: order_creation_id, module_id: 1 }
        //         });
        //         const order_noti_moment = moment().utc().utcOffset(utcOffsetMins);
        //         let order_noti_date_time = order_noti_moment.format("DD/MM/YYYY hh:mm A");
        //         console.log("\n NOTIFIACTION ENTRY FOR ORDER NO:", orderHeader.order_id);
        //         const dorderNotifications = await OrderNotifications.create({
        //             order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
        //             emp_id: dboy_order_mapping.emp_id, role_id: 2, msg_title: 'Order has to be receive',
        //             msg_body: msg_body1, created_at: order_noti_date_time,
        //             latitude: orderHeader.address.latitude, longitude: orderHeader.address.longitude
        //         });
        //         const orderNotifications = await OrderNotifications.create({
        //             order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id,
        //             user_id: orderHeader.user_id, msg_title: 'Order Created Successfully',
        //             msg_body: msg_body, created_at: order_noti_date_time
        //         });

        //         const fetchBody = {
        //             notification: {
        //                 title: "iestre",
        //                 text: msg_body,
        //                 click_action: "com.purpuligo.laundry.NotificationActivity"
        //             },
        //             data: {
        //                 deliveryBoyName: empname,
        //                 orderId: orderHeader.order_id,
        //                 delBoyPhoneNo: emp_phoneno
        //             },
        //             to: login.fcmToken
        //         };
        //         const fetchBody1 = {
        //             notification: {
        //                 title: "iestre",
        //                 text: msg_body1,
        //                 click_action: "com.purpuligo.laundry.NotificationActivity"
        //             },
        //             data: {
        //                 customerName: customername,
        //                 orderId: orderHeader.order_id,
        //                 customerPhoneNo: customerphoneno
        //             },
        //             to: employee_login.fcmToken
        //         };

        //         let result1 = dpushToFirebase(fetchBody1, 3);
        //         console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");

        //         let y = 0;
        //         var notificationId = setInterval(function () {
        //             y = y + 1;
        //             console.log("\n\n TRY TO SEND NOTIFICATION TO FIREBASE :" + y);
        //             let result = pushToFirebase(fetchBody, 3);
        //             if (result) {
        //                 console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER");
        //                 //dpushToFirebase(fetchBody1, 3);
        //                 // console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");
        //                 clearInterval(notificationId);
        //             }
        //             // if (y === 1) {
        //             //     console.log("\n\nNOTIFICATION NOT SEND  SUCCESSFULLY TO DELBOY CUSTOMER");
        //             //     clearInterval(notificationId);
        //             // }
        //         }, 120000);
        //     }
        //     catch (error) {
        //         console.log("\n\n NOTIFICATION ERROR ", error);
        //     }
        // }));

        // send response
        res.send({ status: 1, message: "orders created successfully", data: orderHeaders });
    } catch (err) {
        t.rollback()
        console.log(err);
        res.status(404).send(err);
    }
}


async function validateOrderRequests(orderRequests, createType) {
    console.log('\norder.controller.validateOrderRequests triggered -->');
    const errors = [];
    const { ProductPrice, ProductMaster, ProductStock, ServiceAvailabilityPincode, CartHeader, db1Conn } = await getModels();

    if (createType == 'create') {
        // Cart-Id validation
        let cartFlag = false;
        let cartErrMsg = "";
        for (let i = 0; i < orderRequests.length; i++) {
            const cart_id = orderRequests[i].cart_id;
            const customer_id = orderRequests[i].customer_id;
            let cartHeader = await CartHeader.findOne({ where: { cart_id, customer_id } });
            if (!cartHeader) {
                cartFlag = false;
                cartErrMsg = `Given customer_id :${customer_id} and cart_id:${cart_id} does not exist in cart_header table `
                break;
            }
            cartFlag = true;
        }

        if (!cartFlag) {
            console.log(cartFlag);
            errors.push(cartErrMsg);
        }
    }
    // service-availability(pincode) validation
    // let pincodeFlag = false;
    // let pincodeErrMsg = "";
    // for (let i = 0; i < orderRequests.length; i++) {
    //     const pincode = orderRequests[i].address.pincode;
    //     let serviceAvailable = await ServiceAvailabilityPincode.findOne({ where: { pincode } });
    //     if (!serviceAvailable) {
    //         pincodeFlag = false;
    //         pincodeErrMsg = `service not available ${pincode} `
    //         break;
    //     }
    //     pincodeFlag = true;
    // }

    // if (!pincodeFlag) {
    //     console.log(pincodeFlag);
    //     errors.push(pincodeErrMsg);
    // }

    // price validation
    const productDetails = await ProductMaster.findAll({ include: [{ model: ProductPrice, where: { store_id: orderRequests[0].store_id } }] });
    // productDetails.forEach(product => {
    //     const priceDetails = await ProductPrice.findAll({ where: { prod_id: product.prod_id, store_id: orderRequests[0].store_id } });
    //     product.setDataValue('priceDetails', priceDetails);
    // })


    const productMinPrices = {};
    productDetails.forEach(productDetails => {
        console.log('/\n' + JSON.stringify(productDetails));
        try {
            productMinPrices[productDetails.prod_id] = {
                name: productDetails.prod_name,
                // value: productDetails.product_prices[0].prod_price
            };
        } catch (error) {
            console.log('/\n' + JSON.stringify(productDetails));
            return;
        }
    });
    console.log("productMinPrices===", JSON.stringify(productMinPrices));
    /* const categoryMinPrices = {
    "1": {
        name: "Ironing",
        value: 99,
    },
    "2": {
        name: "Wash & Ironing",
        value: 199,
    },
    "3": {
        name: "Dry Cleaning",
        value: 399,
    },
    };
    */
    const productStockDetails = await ProductStock.findAll();
    const productStocks = {};
    for (let i = 0; i < productStockDetails.length; i++) {
        productStocks[productStockDetails[i].prod_id] = {
            cStock: productStockDetails[i].current_stock
        }
    }

    console.log('product stock ', productStocks);

    let minPriceFlag = false;
    let priceErrMsg = "";
    for (let i = 0; i < orderRequests.length; i++) {
        const allProductDetails = orderRequests[i].order_details;
        // Rs sign ₹
        for (let j = 0; j < allProductDetails.length; j++) {
            const product_Id = allProductDetails[j].prod_id;
            const qty = allProductDetails[j].quantity;
            const product_price = allProductDetails[j].per_unit_price;
            if ((qty > +productStocks[product_Id].cStock)) {
                minPriceFlag = false;
                priceErrMsg = `Minimum price of category ${productMinPrices[product_Id].name}  is Rs. ${productMinPrices[product_Id].value}`
                break;
            }
            minPriceFlag = true;
        }
        if (!minPriceFlag) {
            console.log(priceErrMsg);
            errors.push(priceErrMsg);
            break;
        }
    }

    return errors;

}

exports.orderListById = async (req, res) => {
    console.log('orderListById controller triggered');
    const { OrderHeader, CustomerMaster, OrderStatusMaster, Store, db1Conn } = await getModels();
    try {
        const { customer_id, store_id } = req.body;
        console.log("\nSTORE ID:" + store_id);
        const orders = await OrderHeader.findAll({
            where: {
                store_id: store_id,
                customer_id: customer_id
            },
            order: [['sales_order_id', 'DESC']]
        });
        await Promise.all(orders.map(async (element) => {
            // Select customer_name from customer_master based on customer_id found in sales_order_header
            let customer = await CustomerMaster.findOne({ where: { customer_id: element.customer_id } });

            let storeDetails = await Store.findOne({ where: { store_id: store_id } });

            let statusMaster = await OrderStatusMaster.findOne({ where: { status_id: element.status_id } });

            // console.log("\n\nUSER MASTER DETAILS======", JSON.stringify(customer, 0, 2));
            if (customer) {
                element.setDataValue('customer_name', customer.customer_fname + " " + customer.customer_lname);
            }
            else {
                element.setDataValue('customer_name', 'NA');
            }

            element.setDataValue('status_name', statusMaster.status_desc);
            element.setDataValue('store_name', storeDetails.store_name);

        }));
        if (orders.length > 0) {
            res.json({ status: 1, message: "orders details ", data: orders });
        }
        else {
            res.json({ status: 0, message: "no orders details ", data: [] });
        }
    }
    catch (error) {
        console.log('\nmis_user.controller.salesOrdersList Error triggered -->');
        console.log("\nmis_user.controller.salesOrdersList Error triggered -->" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}

exports.orderDetailsById = async (req, res) => {
    console.log('\norderDetailsById controller triggered -->');
    const { OrderHeader, OrderDetails, CustomerMaster, OrderAddress, ProductMaster, ProductImage, OrderStatusMaster, db1Conn } = await getModels();
    try {
        const { sales_order_id, store_id, customer_id } = req.body;
        console.log("\nSALES ORDER ID:" + sales_order_id);
        console.log("\nSTORE ID:" + store_id);

        let orderHeaderDetails = await OrderHeader.findOne({ where: { sales_order_id: sales_order_id } });

        let addressDetails = await OrderAddress.findOne({ where: { sales_order_id: sales_order_id } });

        const statusDetails = await OrderStatusMaster.findOne({
            where: { status_id: orderHeaderDetails.status_id }
        });

        const salesOrderDetails = await OrderDetails.findAll({
            where: {
                store_id: store_id,
                sales_order_id: sales_order_id,
                customer_id: customer_id
            }
        });

        await Promise.all(salesOrderDetails.map(async (element) => {
            const productDetails = await ProductMaster.findOne({
                where: {
                    prod_id: element.prod_id
                }
            });
            const imageDetails = await ProductImage.findOne({
                where: {
                    prod_id: element.prod_id
                }
            });

            if (imageDetails.prod_image_path == null) {
                element.setDataValue('product_image', '');
            } else {
                element.setDataValue('product_image', imageDetails.prod_image_path);
            }
            element.setDataValue('product_name', productDetails.prod_name);
        }));

        let finalData = { 'headerDetails': orderHeaderDetails, 'orderStatus': statusDetails, 'addressDetails': addressDetails, 'details': salesOrderDetails };

        if (salesOrderDetails) {
            res.json({ status: 1, message: "order details ", data: finalData });
        }
        else {
            res.json({ status: 0, message: "no order details ", data: [] });
        }
    }
    catch (error) {
        console.log('\nmis_user.controller.saleOrderDetails Error triggered -->');
        console.log("\nmis_user.controller.saleOrderDetails Error triggered -->" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: [] });
    }
}