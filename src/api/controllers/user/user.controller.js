const HttpStatus = require('http-status-codes');
const randomize = require('randomatic');
const jwt = require('jsonwebtoken');
const { google } = require('googleapis');
const moment = require('moment');
const axios = require('axios');
const fetch = require('node-fetch');
// local imports
const env = require('../../../env');
const { getModels } = require('../../models');
const jwtSecretKey = '123##$$)(***&';
//const utcOffsetMins = 330;
const utcOffsetMins = env.env.utcOffsetMins;
var format = 'DD/MM/YYYY hh:mm:ss A';
var http = require('http');
var urlencode = require('urlencode');
/*
{
    "authCode" : "4/bwGwqZFwJGPy28QHO9jQ7wOO0RuEunqEuMNrkqZNbW1YZH2dEkKoKdACWEA-uBqdzeoYDR2kz36KHKkjzBsf0DE",
    "accessToken": "ya29.GlsvB-VnqCqeA43kREstN7zWZextJ7-x-kDdj8VyiZz8TK0tA4sQ3kYG0QiGh8KubNJ6VdaRDmMX3KjOWr8wr5w4q99qA4dWpIf6nYSLkGKaIPEx3eTaFLTm57ol",
    "refreshToken": "1\/cy3IeFakEOvXbVJoVGBbSfi7sobfFs2TXQvyRf4ApNc",
}

{
   
 "accessToken": "ya29.ImG1B7dx2HUm_t6L78mLcLd_xUPYgLI0Rt7ezEbeYYxO6peyr73xA69s1zK5BRKec11bpk6dK9RBNsexy21HcHdq6VqJexz6h7f896iRhRna2MMM9X5niiJQbuFMifyVOTDx"
}

*/


// Get App Version Details
exports.getAppVersion = async (req, res) => {
    console.log('\nUserController.getAppVersion triggered -->');
    const { AppVersion, db1Conn } = await getModels();
    try {
        let appDetails = await AppVersion.findOne({ where: { app_version_type: 'Current' } });
        if (appDetails) {
            console.log("\n\nApp Details======", JSON.stringify(appDetails, 0, 2));
            res.json({ status: 1, message: "App Details Send Successfully", data: { appDetails: appDetails } })
        }
        else {
            console.log("\n\nUnable To Send App Detaails");
            res.json({ status: 0, message: "No App Details", data: {} })
        }
    }
    catch (error) {
        console.log('\nUserController.getAppVersion error', error)
        // res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error;
    }
}

// Check App Version Details
exports.checkAppVersion = async (req, res) => {
    console.log('\nUserController.checkAppVersion triggered -->');
    const { AppVersion, db1Conn } = await getModels();
    console.log(`req.body: `, req.body);

    const { app_version_id, app_version_code } = req.body;
    try {
        let appDetails = await AppVersion.findOne({ where: { app_version_type: 'Current' } });
        if (app_version_id >= appDetails.app_version_id) {
            console.log("\n\nUseing  Updated App Details======", JSON.stringify(appDetails, 0, 2));
            res.json({ status: 1, message: "Useing  Updated App", data: { appDetails: appDetails } })
        }
        else {
            console.log("\n\nUnable To Send App Detaails");
            res.json({ status: 0, message: "Please Update App", data: {} })
        }
    }
    catch (error) {
        console.log('\nUserController.checkAppVersion error', error)
        // res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error;
    }
}

exports.googleAndroidAuth = async (req, res) => {
    const { CustomerMaster, Login, db1Conn } = await getModels();
    console.log('\nUserController.googleAndroidAuth triggered -->');

    console.log(`req.body: `, req.body);

    const { accessToken, fcmToken, app_version_id } = req.body;
    console.log(`Access Token: `, accessToken);
    console.log(`fcmToken : `, fcmToken);
    console.log(`app_version_id     :  `, app_version_id);
    const login_type = 'Google';
    let status = "Not Active";
    try {
        // fetch user data from google
        const oAuth2Client2 = new google.auth.OAuth2();
        oAuth2Client2.setCredentials({ access_token: accessToken });
        const userInfo = await google.oauth2({ auth: oAuth2Client2, version: 'v2' }).userinfo.get();
        console.log(`Check existing User googleId...`);

        // Check User googleId exist in login or not
        let user = await CustomerMaster.findOne({ where: { customer_auth: userInfo.data.id } });
        // let user = await CustomerMaster.findOne({ where: { user_auth: '101977992884765235079' } });
        if (user) {
            console.log('User googleId exists in user_master table , Returning User access token...');
            // create an access token
            const access_token = jwt.sign({ id: user.user_id }, jwtSecretKey, {
                expiresIn: '365d', // expires in X secs
            });

            //let lastAccessDate = moment().utcOffset(utcOffsetMins).format(format);
            // const order_moment = moment().utc().utcOffset(utcOffsetMins);
            // var lastAccessDate = order_moment.format("DD/MM/YYYY hh:mm a");
            //const lastAccessDate = moment().utc().utcOffset(utcOffsetMins).format(format);
            let lastAccessDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm A");
            await Login.update(
                { password: access_token, last_access_date: lastAccessDate, fcmToken: fcmToken, app_version_id: app_version_id },
                { where: { user_id: user.user_id } }
            )
            let userLogin = await Login.findOne({ where: { user_id: user.user_id } });
            res.json({ status: 1, message: "existing user_login details", data: userLogin })
        } else {
            console.log(`Check existing User email...`);
            // find if user email exists or not
            const user = await CustomerMaster.findOne({ where: { customer_email: userInfo.data.email, login_type: 'Google' } });
            if (user) {
                console.log('User email exists, Sending Error Response...');
                const errorResponse = {
                    status: 0,
                    message: 'Email ID is in use. For login via Google',
                };
                return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
            } else {
                console.log(`User email doesn't exist, Creating new User...`);

                if (userInfo.data.verified_email) {
                    // activate user
                    status = "Active";

                }
                const t = await db1Conn.transaction();
                try {
                    const userMaster = await CustomerMaster.create(
                        { customer_auth: userInfo.data.id, login_type: login_type, customer_fname: userInfo.data.given_name, customer_lname: userInfo.data.family_name, customer_email: userInfo.data.email, customer_phoneno: "", customer_profile_picture: userInfo.data.picture },
                        { transaction: t });
                    console.log("USER_MASTER ADDED SUCCESSFULLY ")
                    // create an access token
                    const access_token = jwt.sign({ id: userMaster.customer_id }, jwtSecretKey, {
                        expiresIn: '365d', // expires in X secs
                    });

                    let lastAccessDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm A");
                    const loginDetails = await Login.create(
                        { customer_id: userMaster.user_id, password: access_token, last_access_date: lastAccessDate, status: status, fcmToken: fcmToken, app_version_id: app_version_id },
                        { transaction: t });
                    console.log("LOGIN ADDED SUCCESSFULLY ")
                    t.commit()

                    res.json({ status: 1, message: "new user_login details", data: loginDetails })

                }
                catch (error) {
                    t.rollback()
                    console.log(error);
                    //res.status(404).send(error);
                    res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
                }


            }
        }

    } catch (error) {
        console.log(error);
        //res.status(404).send(error);
        res.json({ status: 0, message: "auth error", data: JSON.stringify(error, 0, 2) });
    }
}





// Logout User
exports.logout = async (req, res) => {
    console.log('\nUserController.logout triggered -->');
    const { CustomerMaster, Login, db1Conn } = await getModels();

    /*  {
        user_id: req.user_id
    }
    */
    try {
        let user = await Login.findOne({ where: { customer_id: req.body.customer_id } });
        if (!user) {
            const errorResponse = {
                status: 0,
                message: 'Invalid user',
            };
            return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
        }
        else {
            let deletePassword = await Login.update(
                { password: null },
                { where: { customer_id: req.body.customer_id } }
            );
            const successResponse = {
                status: 1,
                message: 'User logout successful.',
            };
            return res.status(HttpStatus.OK).send(successResponse);
        }

    }
    catch (error) {
        console.log('\nUserController.logout error', error)
        throw error;

    }
}



// Get User Profile
exports.getProfile = async (req, res) => {
    console.log('\nUserController.getProfile triggered -->');
    const { CustomerMaster, db1Conn } = await getModels();

    console.log(`\n req.body: `, req.body);

    const { customer_id } = req.body;
    console.log(`\n User-Id: `, customer_id);

    try {
        let userInfo = await CustomerMaster.findOne({ where: { customer_id: customer_id }, attributes: ['customer_id', 'login_type', 'customer_fname', 'customer_lname', 'customer_email', 'customer_phoneno', 'customer_profile_picture'] });


        if (userInfo) {
            res.json({ status: 1, message: "user profile details", data: userInfo })
        }
        else {
            res.json({ status: 0, message: "user not exist", data: {} })
        }

    }
    catch (error) {
        console.log('\nUserController.getProfile error', error)
        //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error;

    }
}


// Edit User Profile
exports.editProfile = async (req, res) => {
    console.log('\n\n UserController.editProfile triggered -->');
    const { CustomerMaster, db1Conn } = await getModels();

    console.log(`\n\n req.body: `, req.body);

    const { customer_id, login_type, customer_fname, customer_email, customer_phoneno } = req.body;
    console.log(`\n\n User-Id: `, customer_id);
    console.log(`\n\n User-Login-Type: `, login_type);

    try {
        //  let userInfo = await UserMaster.findOne({ where: { user_id: user_id }, attributes: ['user_id', 'login_type', 'user_fname', 'user_lname', 'user_email', 'user_phoneno', 'user_profile_picture'] });
        if (login_type === "Google") {
            await CustomerMaster.update(
                { customer_fname: customer_fname, customer_phoneno: customer_phoneno },
                { where: { customer_id: customer_id } }
            )

            let userInfo = await CustomerMaster.findOne({ where: { customer_id: customer_id }, attributes: ['customer_id', 'login_type', 'customer_fname', 'customer_lname', 'customer_email', 'customer_phoneno', 'customer_profile_picture'] });
            console.log(`\n\n Google Login-Type User Profile Updated.... `);
            res.json({ status: 1, message: "google login user profile updated details", data: userInfo })
        }

    }
    catch (error) {
        console.log('\nUserController.getProfile error', error)
        //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error;

    }
}


exports.userLogin = async (req, res) => {
    const { MisUserLogin, Store, CustomerMaster, Login, db1Conn } = await getModels();
    console.log('\nmis_user.controller.userLogin  triggered -->');

    console.log(`req.body: `, req.body);
    // phoneno is same as otp here
    const { customer_auth, password } = req.body;
    //fcmToken = "fcmToken";
    console.log(`MisUser Login-Id:  `, customer_auth);
    console.log(`MisUser Password:  `, password);
    // console.log(`MisUser Store-Id:  `, store_id);
    // console.log(`MisUser Fcm Token:  `, fcmToken);
    try {
        // Check mis_user given login_id and password match in mis_user_login table or not
        let user = await Login.findOne({ where: { customer_id: customer_auth, password: password } });

        if (user) {

            console.log(`\n\nUser details existing in login table...`);

            let customerDetails = await CustomerMaster.findOne({ where: { customer_auth: user.customer_id } })
            res.json({ status: 1, message: "User details ", data: customerDetails })


        }
        else {
            console.log('\n\n User given login_id and password does not match, Sending Error Response...');
            // const errorResponse = {
            //     status: 0,
            //     message: 'login_id  and password not match. You are unauthenticated',
            //     data: {}
            // };
            // return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
            res.json({ status: 0, message: "login_id  and password not match. You are unauthenticated", data: {} });
        }

    } catch (error) {
        console.log(error);
        //res.status(404).send(error);
        // res.json({ status: 0, message: "auth error", data: JSON.stringify(error, 0, 2) });
        throw error
    }
}


