//const Sequelize = require("sequelize");
const { JsonWebTokenError } = require('jsonwebtoken');
const moment = require('moment');
// local imports
const { getModels } = require('../../models');
/* 
const sequelize = new Sequelize(
    "laundry_db",
    "nodeuser",
    "nodeuser@1234",
    {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000000000,
            operatorsAliases: false
        }
    }

) */
exports.addEditDeleteCart = async (req, res) => {
    const { CartDetails, CartHeader, ProductMaster, db1Conn } = await getModels();
    let flag = 1;
    console.log('\ncart.controller.addCart  triggered -->');
    console.log("\n\n\n\nREQUEST BODY  %%%%===" + JSON.stringify(req.body, 0, 2));
    console.log('\ncart.controller.addCart  Customer_ID : ' + req.body.customer_id);
    let cart_no = Math.floor(100000 + Math.random() * 900000);
    // Check User exist in cart_header or not
    let cart = await CartHeader.findOne({ where: { customer_id: req.body.customer_id } });

    if (cart) {
        console.log("USER EXIST");
        console.log("\n\n\n\nCART %%%%===" + JSON.stringify(cart, 0, 2));
        console.log("\n\n\n\nCART DETAILS %%%%===" + JSON.stringify(req.body.cart_details, 0, 2));
        await Promise.all(req.body.cart_details.map(async (element) => {
            console.log(element);

            let prodMaster = await ProductMaster.findOne({ where: { prod_id: element.prod_id } });
            if (!prodMaster) {
                await CartDetails.destroy({ where: { prod_id: element.prod_id, cart_id: cart.cart_id } });
                console.log("DELETE RECORD FROM CART DETAILS WHOSE PROD-ID NOT EXIST IN PRODUCTMASTER TABLE, PROD-ID :: ", element.prod_id);
            }

            let cartDetail = await CartDetails.findOne({ where: { prod_id: element.prod_id, cart_id: cart.cart_id } });

            if (cartDetail) {

                console.log("CART DETAILS======", JSON.stringify(cartDetail, 0, 2));

                if (element.quantity === 0) {

                    let deleteCart = await CartDetails.destroy({ where: { prod_id: element.prod_id, cart_id: cart.cart_id } });
                    let findCart = await CartDetails.findOne({ where: { cart_id: cart.cart_id } });
                    if (!findCart) {
                        let deleteCartHeader = await CartHeader.destroy({ where: { cart_id: cart.cart_id } });
                        if (deleteCartHeader) {
                            flag = 0;
                        }
                    }

                }
                else {

                    await CartDetails.update(
                        { quantity: element.quantity },
                        { where: { prod_id: element.prod_id, cart_id: cart.cart_id } }
                    )
                }

            }
            else {
                let prodMaster = await ProductMaster.findOne({ where: { prod_id: element.prod_id } });
                if (prodMaster) {
                    await CartDetails.create({ cart_id: cart.cart_id, prod_id: element.prod_id, quantity: element.quantity, category_id: element.category_id });
                    console.log("NEW ENTRY CART DETAILS======", element.prod_id + ":==:" + element.quantity + ":==:" + element.category_id);
                }

            }
        }));

        if (flag) {
            //To be considered
            //  let deleteCart = await CartDetails.destroy({ where: { cart_id: cart.cart_id, quantity: 0 } });
            let updatedCart = await CartHeader.findOne({
                where: { cart_id: cart.cart_id },
                include: [
                    { model: CartDetails, as: 'cart_details' }
                ]
            });
            console.log("UPDATED CART RETURN SUCCESSFULLY ")
            res.json({ status: 1, message: "cart updated successfully", data: updatedCart })
        }
        else {
            console.log("DELETE CART_HEADER AND CART_DETAILS  SUCCESSFULLY ")
            res.json({
                status: 1, message: "no cart", data: {
                    "cart_id": 0,
                    "customer_id": 0,
                    "cart_no": 0,
                    "cart_details": []
                }
            });
        }

        // let updatedCart = await CartHeader.findOne({ where: { user_id: req.body.user_id } });
        // console.log("UPDATED CART RETURN SUCCESSFULLY ")
        // res.json({ status: 1, message: "cart updated successfully", data: updatedCart })
    }
    else {
        const t = await db1Conn.transaction();
        try {

            const cart = await CartHeader.create(
                { customer_id: req.body.customer_id, cart_no: cart_no, cart_details: req.body.cart_details },
                { include: [{ model: CartDetails, as: 'cart_details' }], transaction: t });

            // throw new Error();
            t.commit()
            console.log("CART ADDED SUCCESSFULLY ")
            res.json({ status: 1, message: "cart added successfully", data: cart })

        }
        catch (error) {
            t.rollback()
            console.log(error);
            throw error;
            //res.status(404).send(error);
            //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        }

    }

}
/* exports.findCartsById({
      include: [
          { model: PriceMaster, as: 'price_details' },
          { model: ProductImage, as: 'image_details' },
      ]
  })
      .then(products => {
          res.json(products);
      })
      .catch(error => {
          console.log(error);
          res.status(404).send(error);
      })
} */

// exports.editdeleteCart = async (req, res) => {
//     console.log('\ncart.controller.editdeleteCart  triggered -->');
//     const { CartDetails, CartHeader } = await getModels();
//     //let cartsInfo = await CartDetails.findAll({ where: { cart_id: req.body.cart_id } });
//     //console.log("\n\n\n\nCARTINFO %%%%===" + JSON.stringify(cartsInfo, 0, 2));
//     let cart_id = req.body.cart_id;
//     let flag = 1;
//     await Promise.all(req.body.cart_details.map(async (element) => {
//         console.log("===========================");
//         console.log("\n\n\n\nCART ===" + JSON.stringify(element, 0, 2));
//         console.log(cart_id + "::::" + element.prod_id + ":::" + element.quantity);
//         console.log("===========================");
//         //let cartDetail = await CartDetails.findOne({ where: { prod_id: element.prod_id, cart_id: cart.cart_id } });
//         if (element.quantity > 0) {
//             console.log("CART DETAILS QUANTITY UPDATE ");
//             /*  CartDetails.update(
//                  { quantity: element.quantity },
//                  { where: { prod_id: element.prod_id, cart_id: cart_id } }
//              ).then(() => {
//                  return;
//              }); */
//             let cartDetailUpdate = await CartDetails.update({ quantity: element.quantity }, { where: { prod_id: element.prod_id, cart_id: cart_id } });
//             if (cartDetailUpdate) {
//                 flag = 1;
//             }
//         }
//         else {
//             console.log("CART DETAILS RECORD DELETE FOR  CART_ID: " + element.cart_id + " AND PROD_ID: " + element.prod_id);
//             //  let updatedCart = await CartHeader.findOne({ where: { user_id: req.body.user_id } });

//             let deleteCart = await CartDetails.destroy({ where: { prod_id: element.prod_id, cart_id: cart_id } });

//             /*  CartDetails.destroy({
//                  where: { prod_id: element.prod_id, cart_id: cart_id }
//              }).then(() => {
//                  return;
//              }); */
//             if (deleteCart) {
//                 let findCart = await CartDetails.findOne({ where: { cart_id: cart_id } });
//                 if (!findCart) {
//                     let deleteCartHeader = await CartHeader.destroy({ where: { cart_id: cart_id } });
//                     if (deleteCartHeader) {
//                         flag = 0;
//                     }
//                 }
//             }


//         }

//     }));

//     if (flag) {
//         let updatedCart = await CartHeader.findOne({ where: { cart_id: cart_id } });
//         console.log("UPDATED CART RETURN SUCCESSFULLY ")
//         res.json({ status: 1, message: "cart updated successfully", data: updatedCart })
//     }
//     else {
//         console.log("DELETE CART_HEADER AND CART_DETAILS  SUCCESSFULLY ")
//         res.json({ status: 1, message: "no cart", data: "empty cart" })
//     }

//     /*  if (cartsInfo) {
//          console.log("USER EXIST");
//          console.log("\n\n\n\nCART %%%%===" + JSON.stringify(cart, 0, 2));
//          console.log("\n\n\n\nCART DETAILS %%%%===" + JSON.stringify(req.body.cart_details, 0, 2));
//          await Promise.all(req.body.cart_details.map(async (element) => {
//              console.log(element)
//              let cartDetail = await CartDetails.findOne({ where: { prod_id: element.prod_id, cart_id: cart.cart_id } });
//              if (cartDetail) {
//                  console.log("CART DETAILS======", JSON.stringify(cartDetail, 0, 2));
//                  CartDetails.update(
//                      { quantity: element.quantity + cartDetail.quantity },
//                      { where: { prod_id: element.prod_id, cart_id: cart.cart_id } }
//                  )
//              }
//          }));
//          let updatedCart = await CartHeader.findOne({ where: { user_id: req.body.user_id } });
//          console.log("UPDATED CART RETURN SUCCESSFULLY ")
//          res.json({ status: 1, message: "cart updated successfully", data: updatedCart })
//      } */
// }


exports.findCart = async (req, res) => {
    console.log('\nproduct.controller.findCart  triggered -->');
    console.log(req.params.customer_id);
    const { CartDetails, CartHeader, ProductPrice, ProductMaster, ProductImage } = await getModels();
    try {
        let cart = await CartHeader.findOne({
            where: { customer_id: req.params.customer_id },
            include: [
                { model: CartDetails, as: 'cart_details' }
            ]
        });
        console.log(JSON.stringify(cart), '11111111111');
        //.then(async (cart) => {
        // const cartd = { ...cart };
        // console.log('\nCart. 11111-->' + JSON.stringify(cartd));
        // console.log('\nCart. 22222 -->' + JSON.stringify(cartd.cart_details, 0, 2));
        // cartd.prop = "OK";
        // cartd.set('key', "Hello");
        // console.log('\nCart. 33333-->' + JSON.stringify(cartd.cart, 0, 2));

        if (cart) {
            let cartDetails = cart.cart_details;
            let grand_total_price = 0;
            let grand_total_quantity = 0;

            await Promise.all(cartDetails.map(async (element) => {

                // here select product_name from product_master based on prod_id
                let product_master = await ProductMaster.findOne({ where: { prod_id: element.prod_id } });
                if (product_master) {
                    console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));
                    // here select price from price_master based on prod_id
                    let price_master = await ProductPrice.findOne({ where: { prod_id: element.prod_id, prod_status: '1' } });
                    console.log("PRICE MASTER DETAILS======", JSON.stringify(price_master, 0, 2));
                    console.log('\nproduct ' + element.prod_id + ' price of ' + price_master.prod_price);
                    let quantity = parseInt(element.quantity);
                    console.log('\nProduct quantity ' + quantity);
                    let total_price = parseFloat(price_master.prod_price) * quantity;

                    let product_image = await ProductImage.findOne({ where: { prod_id: element.prod_id } });

                    console.log('\nTotal Price ' + total_price);
                    element.setDataValue('prod_name', product_master.prod_name);
                    element.setDataValue('prod_code', product_master.prod_code);
                    element.setDataValue('per_unit_price', price_master.prod_price);
                    element.setDataValue('total_price', total_price.toFixed(2));
                    element.setDataValue('product_image', product_image.prod_image_path);
                    total_price = parseFloat(total_price.toFixed(2));
                    grand_total_price = total_price + grand_total_price;
                    grand_total_quantity = grand_total_quantity + quantity;
                }
                else {
                    console.log("PRODUCT and PRICE MASTER DETAILS NOT EXIST FOR PRODUCT-ID======", element.prod_id);
                }

            }));

            cart.setDataValue('grand_total_price', grand_total_price.toFixed(2));
            cart.setDataValue('grand_total_quantity', grand_total_quantity);
            res.json({ status: 1, message: "cart details", data: cart });
        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({
                status: 1, message: "cart details", data: {
                    "cart_id": 0,
                    "customer_id": 0,
                    "cart_no": 0,
                    "cart_details": [],
                    "grand_total_price": 0,
                    "grand_total_quantity": 0
                }
            });
        }
    }
    catch (error) {
        console.log("ERROR==" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}