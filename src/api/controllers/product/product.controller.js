const sequelize = require("sequelize");
const Op = sequelize.Op;
const moment = require('moment');
const today = moment();
// local imports
const { getModels } = require('../../models');
const env = require('../../../env');
const { json, where, DATE } = require("sequelize");
const { Store } = require("express-session");
const img_url = env.img.img_url;
// PriceMaster, ProductCategoriesMaster, ProductMaster, ProductImage, ProductPrice, ProductStock, Store
// This method retrive only top level product category like Ironing,Dry Cleaning, Wash and Ironing
exports.findCategory = async (req, res) => {
    console.log('\nproduct.controller.findCategory  triggered -->');
    const { ProductCategoriesMaster } = await getModels();

    ProductCategoriesMaster.findAll({
        where: { parent_id: 0 }
    }).then(products => {
        res.json(products);
    })
        .catch(error => {
            console.log(error);
            res.status(404).send(error);
        })

}

exports.findSubCategoryOrProductList = async (req, res) => {
    console.log('\nproduct.controller.findSubCategoryOrProductList  triggered -->');

    try {
        const { ProductCategoriesMaster, ProductMaster, ProductPrice, ProductImage, ProductStock, Store } = await getModels();
        const data = {};
        const reqBody = req.body;
        const categories = await ProductCategoriesMaster.findAll({ where: { parent_id: reqBody.id } });
        if (categories.length > 0) {
            data.category_type = "SubCategory List";
            data.category_type_id = 1; // For Subcategory List
            data.list = categories;
        }
        else {
            const products = await ProductMaster.findAll({ where: { prod_category_id: reqBody.id } });
            if (products.length > 0) {
                await Promise.all(products.map(async (product) => {
                    const productPrice = await ProductPrice.findOne({
                        where: { store_id: reqBody.store_id, prod_id: product.prod_id }
                    });

                    const productStock = await ProductStock.findOne({
                        where: { store_id: reqBody.store_id, prod_id: product.prod_id }
                    });

                    const productImage = await ProductImage.findOne({
                        where: { prod_id: product.prod_id }
                    });
                    const store = await Store.findOne({
                        where: { store_id: reqBody.store_id }
                    });
                    product.setDataValue('prod_price', productPrice.prod_price);
                    product.setDataValue('prod_stock', productStock.current_stock);
                    product.setDataValue('prod_image', productImage.prod_image_path);
                    product.setDataValue('prod_store_name', store.store_name);
                }));
                data.category_type = "Product List";
                data.category_type_id = 2; // For Product List
                data.list = products;
            }
        }
        res.json({ status: 1, message: "subcategories or products list", data: data });
    }
    catch (error) {
        console.log(error);
        res.status(404).json({ status: 0, message: "unable to fetch subcategory or products list , try again " });
    }
}

exports.updateProductPrice = async (req, res) => {
    console.log('\nproduct.controller.updateProductPrice  triggered -->');
    const reqBody = req.body;
    const flag = false;
    const { ProductPrice } = await getModels();
    await Promise.all(reqBody.map(async (element) => {
        const productExist = await ProductPrice.findAll({
            where: {
                store_id: element.store_id,
                prod_id: element.prod_id
            },
            attributes: ['product_price_id']
        });
        if (productExist.length != 0) {
            await ProductPrice.update({ prod_price: element.new_price },
                {
                    where: {
                        store_id: element.store_id,
                        prod_id: element.prod_id
                    }
                });
        } else {
            flag = true;
            return;
        }
    }));
    if (!flag) {
        res.json({ status: 1, message: "Product Price has been updated" });
    } else {
        res.json({ status: 0, message: "Product Price has been not updated" });
    }
}

exports.updateProductStock = async (req, res) => {
    console.log('\nproduct.controller.updateProductStock  triggered -->');
    const { ProductStock, ProductStockMove, db1Conn } = await getModels();
    const reqBody = req.body;
    console.log((+reqBody.total_stock - +reqBody.waste_stock));
    const t = await db1Conn.transaction();
    try {
        const previousValue = await ProductStock.findOne({ where: { store_id: reqBody.store_id, prod_id: reqBody.prod_id }, attributes: ['current_stock', 'total_waste'] });
        console.log(JSON.stringify(previousValue));
        const prdStock = await ProductStock.findOne({
            where: {
                prod_id: reqBody.prod_id,
                store_id: reqBody.store_id,
            }
        });
        console.log(JSON.stringify(prdStock));
        const reduceStock = await ProductStock.update(
            {
                current_stock: +prdStock.current_stock + (Number(reqBody.total_stock) - Number(reqBody.waste_stock)),
                total_waste: prdStock.total_waste + reqBody.waste_stock
            },
            {
                where: { store_id: reqBody.store_id, prod_id: reqBody.prod_id }
            },
            { transaction: t }
        );
        console.log(JSON.stringify(reduceStock));
        var date = new Date();
        const udateByMovId = await ProductStockMove.create(
            {
                store_id: reqBody.store_id,
                product_id: reqBody.prod_id,
                mov_type: 1,
                mov_quantity: reqBody.total_stock - reqBody.waste_stock,
                mov_sku: 1,
                sales_order_id: 1,
                sales_date: today.format('DD/MM/YYYY'),
                sales_time: today.format('HH:MM'),
                close_stock: 1,
                close_stock_sku: 1,
                open_stock: 1,
                open_stock_sku: 1,
                waste: reqBody.waste_stock,
                purchase_price: reqBody.total_stock * reqBody.unit_price
            },
            { transaction: t }
        );
        console.log(JSON.stringify(udateByMovId));
        t.commit();
        res.json({ status: 1, message: "Product Stock has been updated" });
    } catch (error) {
        t.rollback();
        res.json({ status: 0, message: "Product Stock has been not updated" });
        console.log(error);
    }
}

exports.productDetailsById = async (req, res) => {
    console.log('productDetailsById controller is triggered');
    const reqBody = req.body;
    const { ProductPrice, ProductStock, ProductMaster, ProductImage, Store } = await getModels();
    try {
        const productDtails = await ProductMaster.findOne(
            {
                where: { prod_id: reqBody.prod_id },
                include: [
                    {
                        model: ProductPrice,
                        where: { prod_id: reqBody.prod_id, store_id: reqBody.store_id }
                    },
                    {
                        model: ProductStock,
                        as: 'stock_details',
                        where: { prod_id: reqBody.prod_id }
                    },
                    {
                        model: ProductImage,
                        as: 'image_details',
                        where: { prod_id: reqBody.prod_id }
                    }
                ]
            });
        const storeDetails = await Store.findOne({
            where: { store_id: reqBody.store_id }
        })
        let finalResult = { 'storeDetails': storeDetails, 'productDetails': productDtails };
        console.log(finalResult);
        res.json({ status: 0, message: "Product Details Found", data: finalResult });
    } catch (error) {
        console.log(error);
        res.json({ status: 0, message: "Product Details Not Found" });
    }
}
