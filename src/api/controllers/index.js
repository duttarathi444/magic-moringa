'use strict';
// export all controllers
exports.UserController = require('./user/user.controller');
exports.ProductController = require('./product/product.controller');
exports.CartController = require('./cart/cart.controller');
exports.OrderController = require('./order/order.controllers');
exports.OrderNotificationsController = require('./order/order.notifications.controller');
exports.AddressController = require('./address/address.controller');
exports.LaundryManController = require('./employee/laundryman.controller');
exports.DeliveryBoyController = require('./employee/deliveryboy.controller');
exports.MisUserController = require('./employee/mis_user.controller');
